import React from 'react';
import './App.css';
import {Col, Container, Row} from "react-bootstrap";


function App() {
    const ACCOUNT = "account";
    const ENTRY_PRICE = "entryPrice";
    const TP = "tp";
    const SL = "sl";
    const POSITION_OVERRIDE = "positionOverride";

    const [account, setAccount] = React.useState(localStorage.getItem(ACCOUNT));
    const [entryPrice, setEntryPrice] = React.useState(localStorage.getItem(ENTRY_PRICE));
    const [tp, setTP] = React.useState(localStorage.getItem(TP));
    const [sl, setSL] = React.useState(localStorage.getItem(SL));
    const [positionOverride, setPositionOverride] = React.useState(localStorage.getItem(POSITION_OVERRIDE));


    const handle = (event) => {
        const value = event.target.value;
        const key = event.target.id;
        localStorage.setItem(key, value)
        switch (key) {
            case ACCOUNT: {
                setAccount(value);
                break;
            }
            case ENTRY_PRICE: {
                setEntryPrice(value);
                break;
            }
            case TP: {
                setTP(value);
                break;
            }
            case SL: {
                setSL(value);
                break;
            }
            case POSITION_OVERRIDE: {
                setPositionOverride(value);
                break;
            }
            default: {
                alert('Not implemented: ' + key + ' with value: ' + value);
            }
        }
    };

    const computedPosition = (parseInt(account) / parseFloat(entryPrice)).toFixed(4);
    let position = positionOverride > 0 ? positionOverride : computedPosition;

    const closePositionInUsd = (position * parseFloat(tp)).toFixed(2);
    const closePositionInUsdWhenSL = (position * parseFloat(sl)).toFixed(2);
    const inPosition = position * entryPrice

    let profit = closePositionInUsd - inPosition;
    let loss = closePositionInUsdWhenSL - inPosition;
    if (tp<sl){
        // is short
        profit *= (-1);
        loss *= (-1);
    }
    let rr = Math.abs((profit / loss).toFixed(3));
    return (

        <div className="App">
            <header>
                <h1 className="m-4">Position calculator</h1>
            </header>
            <Container role='main'>
                <Row className="justify-content-md-center ">
                    <Col className="col-12">
                        <form id='js-form' className="justify-content-end">
                            <div className="mt-2 mb-2">
                                <label htmlFor={ACCOUNT} className="d-flex align-left">Account size</label>
                                <input
                                    id={ACCOUNT}
                                    type="number"
                                    inputMode="decimal"
                                    className="form-control"
                                    value={account}
                                    onChange={handle}
                                />
                            </div>
                            <div className="mt-2 mb-2">
                                <label htmlFor={ENTRY_PRICE} className="d-flex align-left">Entry price</label>
                                <input
                                    id={ENTRY_PRICE}
                                    type="number"
                                    inputMode="decimal"
                                    className="form-control"
                                    value={entryPrice}
                                    onChange={handle}
                                />
                            </div>


                            <div className="d-flex justify-content-center m-4">
                                <b>Position size : {computedPosition}</b>
                            </div>


                            <div className="mb-2 align-right">
                                <label htmlFor={POSITION_OVERRIDE} className="d-flex align-left text-muted">Position
                                    override</label>
                                <input
                                    id={POSITION_OVERRIDE}
                                    type="number"
                                    inputMode="decimal"
                                    className="form-control"
                                    value={positionOverride}
                                    onChange={handle}
                                />
                            </div>
                            <div className="d-flex">
                                <div className="mt-2 mb-2 col-6">
                                    <label htmlFor={SL} className="d-flex align-left text-danger">Stop loss</label>
                                    <input
                                        id={SL}
                                        type="number"
                                        inputMode="decimal"
                                        className="form-control"
                                        value={sl}
                                        onChange={handle}
                                    />
                                </div>
                                <div className="mt-2 mb-2 col-6">
                                    <label htmlFor={TP} className="d-flex align-left text-primary">Take profit</label>
                                    <input
                                        id={TP}
                                        type="number"
                                        inputMode="decimal"
                                        className="form-control"
                                        value={tp}
                                        onChange={handle}
                                    />
                                </div>
                            </div>
                            <div className="d-flex justify-content-center m-4">
                                <b>Sell : {closePositionInUsd}</b><br/>
                            </div>
                            <div className="justify-content-center m-3">
                                RR: {rr}<br/>
                                {printProfit(profit, 1, loss, inPosition, position)}
                                {printProfit(profit, 10, loss, inPosition, position)}
                                {printProfit(profit, 20, loss, inPosition, position)}
                                {printProfit(profit, 100, loss, inPosition, position)}
                            </div>

                        </form>
                    </Col>
                </Row>
            </Container>
        </div>
    );

}

function printProfit(profit, leverage, loss, inPosition, position) {
    profit = (profit * leverage).toFixed(2);
    if (!isNaN(loss)) loss = (loss * leverage).toFixed(2);
    return (
        <div>
            Profit {leverage === 1 ? <i>({(profit/(inPosition/100)).toFixed(2)}%)</i> : <i>with {leverage}x <small>({Math.floor(position*leverage)})</small></i>} :
            <b> ${profit}</b>
            {(isNaN(loss) ? '' : <i style={{whiteSpace: "nowrap"}}> (possible loss {loss})</i>)}<br/>
        </div>
    );

}

export default App;
